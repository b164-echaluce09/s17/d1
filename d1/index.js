console.log("Hello World");


//Arrays and Indexes
//store multiple values in a single variable.
//[] ----> Array Literals 


//Common Examples of arrays

let grades = [98.5, 91.2, 93.1, 89.0];
console.log(grades[0])

//Alternative way to write arrays
let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake express js'
];


//Reassigning array values
console.log('Array before reassignment');
console.log(myTasks);
myTasks[0] = 'hello world';
console.log('Array after reassignment');
console.log(myTasks);
//Accessing an array element that does not exist will return "undefined"
console.log(myTasks[4]);


//Getting the length of an array

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Toshiba', 'Gateway', 'Redfox', 'Fujitsu'];

console.log(computerBrands.length);
console.log(computerBrands)


if(computerBrands.length > 5) {
	console.log('We have too many suppliers. Please coordinate with the operations manager.')
}

//Access the last element of an array
let lastElementIndex = computerBrands.length - 1;
console.log(computerBrands[lastElementIndex]);

//ARRAY METHODS

//Mutator Methods
	// are functions that 'mutate' or change an array after they're created.


let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];



//push()
/*
Adds an element in the end of an array AND returns array's length
	syntax:
		arrayName.push();
*/

console.log('Current Array: ');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method');
console.log(fruits);

//Add elements
fruits.push('Avocado', 'Guava')
console.log('Mutated array from push method');
console.log(fruits);



//pop()
/*
-Removes the last element in an array AND returns the removed element
Syntax:
	arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);

fruits.pop();
console.log(fruits);

//unshift()
/*
- Adds one or more elements at the beginning of an array
Syntax:

	arrayName.unshift('elementA', 'elementB')
*/

fruits.unshift('Lime', 'Banana');

console.log('Mutated array from unshift method');
console.log(fruits);


//shift()
/*
- Removes an element at the beginning of an array AND returns the removed element
Syntax:
	arrayName.shift()
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);


//splice()
/*
- Simultaneously removes elements from a specified index number and adds elements
Syntax;
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/
fruits.splice(3, 2, 'Lime', 'Cherry');
console.log('splice method:');
console.log(fruits);


//sort()
/*
- Rearranges the array elements in alphanumeric order.

arrayName.sort();
*/

fruits.sort();
console.log('sort method');
console.log(fruits);


//reverse()
/*
- Reverses the order of array elements.

arrayName.reverse()
*/
fruits.reverse();
console.log("reverse method");
console.log(fruits);


//Non-Mutator Methods
	//methods that are functions that do not modify or change an array after they're created.
	//it returns elements from an array and combining arrays and printing the output

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];


//indexOf()
/*
	- returns the index number of the first matching element found in an array
	- if no match was found, the result will be -1
	- The search process will be done from the first element proceeding to the last element

Syntax;
	arrayName.indexOf(searchValue)
	arrayName.indexOf(searchValue, fromIndex)
*/

let firstIndex = countries.indexOf('PH');
console.log(`Result of indexOf method: ${firstIndex}`);

let invalidCountry = countries.indexOf('BR');
console.log(`indexOf: ${invalidCountry} ` );

//lastIndexOf();
/*
- returns the index number of the LAST matching element found in an array
- search process is from last element proceeding to the first element

arrayName.lastIndexOf(searchValue)
*/

let lastIndex = countries.lastIndexOf('PH');
console.log(`lastIndexOf method: ${lastIndex}`); //5


//getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 4);
console.log(`lasIndexOf: ${lastIndexStart}`);


//slice()
/*
- Portions/slices elements from an array AND returns new array
Syntax:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, endingIndex);
*/

//slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log(countries);
console.log("slice method:");
console.log(slicedArrayA);


//slicing off elements starting from a specified index to another index
let slicedArrayB = countries.slice(2, 3);
console.log(slicedArrayB)

//starting from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);



//toString()
/*
	- returns an array as a string separated by commas
*/

let stringArray = countries.toString();
console.log("toString method:");
console.log(stringArray);

//concat()

/*
- combines two arrays and returns the combined result
Syntax:
	arrayA.concat(arrayB);
	arrayA.concat(elementA);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA[0].concat(tasksArrayB[0]);
console.log(`concat method modified ${tasks}`)
console.log(tasks);


//combining multiple arrays
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);


//combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log(combinedTasks);


//join()
/*
- returns an array as a string separated by specified separator string
Syntax:
	arrayName.join('separatorString');
*/

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join()); //default comma
console.log(users.join(' ')); 
console.log(users.join(' - ')); 


//Iteration Methods
/*
- iteration methods are loops designed to perform repetitive tasks on arrays
- useful for manipulating array data resulting in complex tasks
*/


//forEach()
/*
- similar to a for loop that iterates on each array element
- normally work with a function supplied as an argument

Syntax:
	arrayName.forEach(function(indivElement) {
		statement
	})
*/


allTasks.forEach(function(task) {
	console.log(task);
});


//mini activity
//print out the allTasks using the for loop


for(i = 0; i < allTasks.length; i++){
	console.log(allTasks[i])
}


//Using forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task) {
	//if element/string's length is greater than 10 characters, push it to the filteredTasks
	if(task.length > 10 ) {
		//add the element to the filteredTasks array
		filteredTasks.push(task);
	}
});

console.log("Result of filtered tasks:")
console.log(filteredTasks);


//sample with prompt
let sampleArray = ['eat', 'drink'];
let data = prompt("add a data");
let firstName = prompt("add firstName")
sampleArray.unshift(data);
sampleArray.push(firstName);
console.log(sampleArray);

//
// [1, 2, 3, 4, 5]
// arrayName.forEach(function(numbers) {
// 	if(numbers % 2 === 0) {
// 		console.log("i am even number" + numbers)
// 	} else {
// 		console.log("i am an odd number" + numbers)
// 	}
// })


//map()
/*
- Iterates on each element AND returns new array with different values depending on the result of the function's operation
- This is useful for performing tasks where mutating/changing the elements are required
- Unlike forEach method, the map method requires the use of a 'return' statement in order to create another array with the performed operation/statement

Syntax:
	let/const resultArray = arrayName.map(function(indivElement) {
		return statement
	})
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
	return number * number
})

console.log("Map method:");
console.log(numbers);
console.log(numberMap); //returns new array


//every()
/*
- checks if ALL elements in an array meet the given condition
- This is useful for validating data stored in arrays especially when dealing with large amounts of data.
- Returns a true value if all elements meet the condition and false if otherwise
Syntax:
	let/const resultArray = arrayName.every(function(indivElement) {
		return expression/condition
	})
*/

let allValid = numbers.every(function(number) {
	return (number > 0);
})

console.log("every method:")
console.log(allValid); 


//some()
/*
- checks if atleast ONE element in the array meets the given conditon
- return a boolean value
*/

let someValid = numbers.some(function(number) {
	return (number < 3)
})

console.log("some method")
console.log(someValid);

//combining the returned result from the every/some method may be used in other statements (if else) to perform consecutive results
if(someValid) {
	console.log('Some of numbers in the array are greater than 2');
}


if(allValid) {
	console.log('ALL of numbers in the array are greater than 0');
}



//filter()

/*
- returns new array that contains elements which meets the given condition
- returns an empty array if no elements were found
- useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods

Syntax:
	let/const resultArray = arrayName.filter(function(indivElement) {
		return expression/condition
	})

*/

let filterValid = numbers.filter(function(number) {
	return (number < 3);
})

console.log("filter method: ");
console.log(filterValid);

let nothingFound = numbers.filter(function(number) {
	return (number == 0)
})

console.log(nothingFound);


//Filtering using forEach
let filteredNumbers = [];


numbers.forEach(function(number) {
	if(number < 3) {
		filteredNumbers.push(number);
	}
})

console.log(filteredNumbers)



//another example using the filter

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes('a')
})

console.log(filteredProducts);
//Methods can be "chained" using them one after another
//The result of the first method is used on the second method until all "chained methods have been resolved"

//How chaining resolves in our example:
//1. The "product" element will be converted into all lowercase letters.
//2. the resulting lowercased string is used in the "includes" method






//reduce()
/*
- evaluates elements from left to right and returns/reduces the array into single value

Syntax:
	let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
		return expression/operation
	})

- accumulator paramater in the function stores the result for every iteration of the loop

-currentValue is the current/next element in the array that is evaluated in each iteration of the loop
*/
//[1, 2, 3, 4, 5]
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {
	//track the current iteration count and accumulator/currentValue data
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);


	//the operation to reduce the array into a single value
	return x + y
})

console.log("result of reduce method: " + reducedArray);


//reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y) {
	return x + ' ' + y;
})

console.log("reduce method: " + reducedJoin);

//Multidimensional Arrays
//it is for complex data structures

let chessBoard = [
['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],

];

console.log(chessBoard[0][2]);